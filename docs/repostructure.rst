:orphan:

.. _repostructure:

Repository structure
--------------------

``master`` branch contains latest stable release of the package.
``devel`` branch is a staging branch for the next release.

Releases are
`tagged <https://gitlab.com/datadrivendiscovery/d3m/tags>`__.
