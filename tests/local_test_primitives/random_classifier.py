import os
import random
import typing

from d3m import container, exceptions, utils
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces.base import CallResult
# from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase


__all__ = ('RandomClassifier',)


Inputs = typing.Union[container.DataFrame, container.List, container.ndarray]
Outputs = typing.Union[container.DataFrame, container.List, container.ndarray]


class Hyperparams(hyperparams.Hyperparams):
    pass


# class RandomClassifier(SupervisedLearnerPrimitiveBase[Inputs, Outputs, None, Hyperparams]):
class RandomClassifier(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which extracts a DataFrame out of a Dataset.
    """

    metadata = metadata_base.PrimitiveMetadata({
        'id': 'b8d0d982-fc53-4a3f-8a8c-a284fdd45bfd',
        'version': '0.1.0',
        'name': "Random Classifier",
        'python_path': 'd3m.primitives.classification.random_forest.Test',
        'installation': [{
           'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/tests-data.git@{git_commit}#egg=test_primitives&subdirectory=primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.BINARY_CLASSIFICATION,
            metadata_base.PrimitiveAlgorithmType.MULTICLASS_CLASSIFICATION
        ],
        'primitive_family': metadata_base.PrimitiveFamily.CLASSIFICATION,
        'source': {
            'name': 'test',
            'contact': 'mailto:author@example.com',
            'uris': [
                # Unstructured URIs. Link to file and link to repo in this case.
                'https://gitlab.com/datadrivendiscovery/tests-data/blob/master/primitives/test_primitives/random_classifier.py',
                'https://gitlab.com/datadrivendiscovery/tests-data.git',
            ],
        },
    })

    def __init__(
        self, *, hyperparams: Hyperparams, random_seed: int = 0
    ) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        self._random = random.Random()
        self._random.seed(random_seed)
        self._training_inputs = None
        self._fitted = False
        self._classes = None

    def set_training_data(self, *, inputs: Inputs) -> None:
        self._training_inputs = inputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        if isinstance(self._training_inputs, container.DataFrame):
            self._classes = self._training_inputs.ix[:,0].unique().tolist()
        elif isinstance(self._training_inputs, container.List):
            self._classes = list(set(self._training_inputs))
        elif isinstance(self._training_inputs, container.ndarray):
            self._classes = list(set(self._training_inputs[:, 0]))
        else:
            raise exceptions.InvalidArgumentTypeError('Invalid training outputs type: {}'.format(
                type(self._training_inputs)
            ))

        self._fitted = True

        return CallResult(None)


    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if not self._fitted:
            raise exceptions.InvalidStateError('not fitted')

        k = len(inputs)
        predictions = self._random.choices(self._classes, k=k)

        if isinstance(self._training_inputs, container.DataFrame):
            result = container.DataFrame({'predictions': predictions})
        elif isinstance(self._training_inputs, container.List):
            result = predictions
        elif isinstance(self._training_inputs, container.ndarray):
            result = container.ndarray.reshape(predictions, (k, 1))

        return CallResult(result)
