import unittest

import jsonschema
import numpy

from d3m import container
from d3m.metadata import base


def copy_elements_metadata(source_metadata, target_metadata, from_selector, to_selector=(), *, ignore_all_elements=False):
    return source_metadata._copy_elements_metadata(target_metadata, list(from_selector), list(to_selector), [], ignore_all_elements)


class TestMetadata(unittest.TestCase):
    def test_basic(self):
        md1 = base.Metadata({'value': 'test'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})

        md2 = md1.update((), {'value2': 'test2'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})

        md3 = md2.update(('foo',), {'element': 'one'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})

        md4 = md3.update((base.ALL_ELEMENTS,), {'element': 'two'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})

        md5 = md4.update(('foo',), {'element': 'three'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})

        md6 = md5.clear()

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})
        self.assertEqual(md6.query(()), {})
        self.assertEqual(md6.query(('foo',)), {})
        self.assertEqual(md6.query(('bar',)), {})

        md7 = md6.update((), {'value': 'test'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})
        self.assertEqual(md6.query(()), {})
        self.assertEqual(md6.query(('foo',)), {})
        self.assertEqual(md6.query(('bar',)), {})
        self.assertEqual(md7.query(()), {'value': 'test'})
        self.assertEqual(md7.query(('foo',)), {})
        self.assertEqual(md7.query(('bar',)), {})

        md8 = md7.clear({'value2': 'test2'})

        self.assertEqual(md1.query(()), {'value': 'test'})
        self.assertEqual(md1.query(('foo',)), {})
        self.assertEqual(md1.query(('bar',)), {})
        self.assertEqual(md2.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md3.query(('foo',)), {'element': 'one'})
        self.assertEqual(md4.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md4.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md4.query(('foo',)), {'element': 'two'})
        self.assertEqual(md5.query(()), {'value': 'test', 'value2': 'test2'})
        self.assertEqual(md5.query((base.ALL_ELEMENTS,)), {'element': 'two'})
        self.assertEqual(md5.query(('foo',)), {'element': 'three'})
        self.assertEqual(md6.query(()), {})
        self.assertEqual(md6.query(('foo',)), {})
        self.assertEqual(md6.query(('bar',)), {})
        self.assertEqual(md7.query(()), {'value': 'test'})
        self.assertEqual(md7.query(('foo',)), {})
        self.assertEqual(md7.query(('bar',)), {})
        self.assertEqual(md8.query(()), {'value2': 'test2'})
        self.assertEqual(md8.query(('foo',)), {})
        self.assertEqual(md8.query(('bar',)), {})

    def test_all_elements(self):
        md1 = base.Metadata()

        md2 = md1.update((base.ALL_ELEMENTS, 'bar'), {'value': 'test1'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})

        md3 = md2.update(('foo', 'bar'), {'value': 'test2'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})

        md4 = md3.update((base.ALL_ELEMENTS, 'bar'), {'value': 'test3'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})

        md5 = md4.update(('foo', base.ALL_ELEMENTS), {'value': 'test4'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})

        md6 = md5.update(('foo', 'bar'), {'value': 'test5'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})
        self.assertEqual(md6.query(('foo', 'bar')), {'value': 'test5'})

        md7 = md6.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'value': 'test6'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})
        self.assertEqual(md6.query(('foo', 'bar')), {'value': 'test5'})
        self.assertEqual(md7.query(('foo', 'bar')), {'value': 'test6'})

        md8 = md7.update(('foo', 'bar'), {'value': 'test7'})

        self.assertEqual(md2.query(('foo', 'bar')), {'value': 'test1'})
        self.assertEqual(md3.query(('foo', 'bar')), {'value': 'test2'})
        self.assertEqual(md4.query(('foo', 'bar')), {'value': 'test3'})
        self.assertEqual(md5.query(('foo', 'bar')), {'value': 'test4'})
        self.assertEqual(md6.query(('foo', 'bar')), {'value': 'test5'})
        self.assertEqual(md7.query(('foo', 'bar')), {'value': 'test6'})
        self.assertEqual(md8.query(('foo', 'bar')), {'value': 'test7'})

        self.assertEqual(md8.to_json_structure(), [{
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'value': 'test6'
            }
        }, {
            'selector': ['foo', 'bar'],
            'metadata': {
                'value': 'test7'
            }
        }])

    def test_removal(self):
        md1 = base.Metadata().update((), {'value': 'test1'})

        self.assertEqual(md1.query(()), {'value': 'test1'})

        md2 = md1.update((), {'value': base.NO_VALUE})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md2.query((), ignore_all_elements=True), {})

        md3 = md2.update((), {'value': {'value2': 'test2'}})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md3.query(()), {'value': {'value2': 'test2'}})

        md4 = md3.update((), {'value': {'value2': base.NO_VALUE}})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md3.query(()), {'value': {'value2': 'test2'}})
        self.assertEqual(md4.query(()), {})

        md5 = md4.update((), {'value': base.NO_VALUE})

        self.assertEqual(md1.query(()), {'value': 'test1'})
        self.assertEqual(md2.query(()), {})
        self.assertEqual(md3.query(()), {'value': {'value2': 'test2'}})
        self.assertEqual(md4.query(()), {})
        self.assertEqual(md5.query(()), {})

    def test_empty_dict(self):
        md = base.Metadata().update((), {'value': {}})

        self.assertEqual(md.query(()), {'value': {}})

        md = md.update((), {'value': {'a': '1', 'b': 2}})

        self.assertEqual(md.query(()), {'value': {'a': '1', 'b': 2}})

        md = md.update((), {'value': {'a': base.NO_VALUE, 'b': base.NO_VALUE}})

        self.assertEqual(md.query(()), {})

        md = md.update((), {'value': {'a': '1', 'b': 2}})

        self.assertEqual(md.query(()), {'value': {'a': '1', 'b': 2}})

        md = md.update((), {'value': {'a': base.NO_VALUE}})

        self.assertEqual(md.query(()), {'value': {'b': 2}})

    def test_remove(self):
        metadata = base.Metadata().update((), {'value': 'test1'})
        metadata = metadata.update(('a',), {'value': 'test2'})
        metadata = metadata.update(('a', 'b'), {'value': 'test3'})
        metadata = metadata.update(('a', 'b', 'c'), {'value': 'test4'})
        metadata = metadata.update((base.ALL_ELEMENTS, 'b', 'd'), {'value': 'test5'})
        metadata = metadata.update((base.ALL_ELEMENTS, 'b', 'e', base.ALL_ELEMENTS), {'value': 'test6'})

        self.assertEqual(metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b'],
                'metadata': {
                    'value': 'test3',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove(('a', 'b'))

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove(('a', 'b'), recursive=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
        ])

        new_metadata = metadata.remove((), recursive=True)

        self.assertEqual(new_metadata.to_json_structure(), [])

        new_metadata = metadata.remove((base.ALL_ELEMENTS, 'b'))

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove((base.ALL_ELEMENTS, 'b'), recursive=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
        ])

        new_metadata = metadata.remove((base.ALL_ELEMENTS, 'b'), strict_all_elements=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b'],
                'metadata': {
                    'value': 'test3',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove((base.ALL_ELEMENTS, 'b'), recursive=True, strict_all_elements=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b'],
                'metadata': {
                    'value': 'test3',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove(('a', base.ALL_ELEMENTS))

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove(('a', base.ALL_ELEMENTS), strict_all_elements=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b'],
                'metadata': {
                    'value': 'test3',
                },
            },
            {
                'selector': ['a', 'b', 'c'],
                'metadata': {
                    'value': 'test4',
                },
            },
        ])

        new_metadata = metadata.remove(('a', base.ALL_ELEMENTS), recursive=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'd'],
                'metadata': {
                    'value': 'test5',
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
        ])

        new_metadata = metadata.remove((base.ALL_ELEMENTS, 'b', base.ALL_ELEMENTS))

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', 'b', 'e', '__ALL_ELEMENTS__'],
                'metadata': {
                    'value': 'test6',
                },
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b'],
                'metadata': {
                    'value': 'test3',
                },
            },
        ])

        new_metadata = metadata.remove((base.ALL_ELEMENTS, 'b', base.ALL_ELEMENTS), recursive=True)

        self.assertEqual(new_metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'value': 'test1',
                }
            },
            {
                'selector': ['a'],
                'metadata': {
                    'value': 'test2',
                },
            },
            {
                'selector': ['a', 'b'],
                'metadata': {
                    'value': 'test3',
                },
            },
        ])

    def test_remove_column(self):
        metadata = base.DataMetadata().update((base.ALL_ELEMENTS, 0), {'name': 'column1'})
        metadata = metadata.update((base.ALL_ELEMENTS, 1), {'name': 'column2'})
        metadata = metadata.update((10, 0), {'value': 'row10.0'})
        metadata = metadata.update((10, 1), {'value': 'row10.1'})

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {'name': 'column1'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'column2'},
        }, {
            'selector': [10, 0],
            'metadata': {'value': 'row10.0'},
        }, {
            'selector': [10, 1],
            'metadata': {'value': 'row10.1'},
        }])

        metadata = metadata.remove_column(0)

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {'name': 'column2'},
        }, {
            'selector': [10, 1],
            'metadata': {'value': 'row10.1'},
        }])

    def test_check(self):
        data = container.Dataset({
            '0': container.ndarray(numpy.array([
                [1, 2, 3],
                [4, 5, 6],
            ])),
        })

        md1 = base.DataMetadata().update((), {
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': type(data),
            'value': 'test'
        })

        md1.check(data)

        md2 = md1.update(('missing',), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md2.check(data)

        md3 = md1.update(('0', 1), {'value': 'test'})

        md4 = md3.update(('0', 2), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md4.check(data)

        md5 = md3.update(('0', 1, 3), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'cannot be resolved'):
            md5.check(data)

        md6 = md3.update(('0', 1, 2, base.ALL_ELEMENTS), {'value': 'test'})

        with self.assertRaisesRegex(ValueError, 'ALL_ELEMENTS set but dimension missing at'):
            md6.check(data)

    def test_errors(self):
        with self.assertRaisesRegex(TypeError, 'Metadata should be a dict'):
            base.Metadata().update((), None)

        class Custom:
            pass

        with self.assertRaisesRegex(TypeError, 'is not known to be immutable'):
            base.Metadata().update((), {'foo': Custom()})

        with self.assertRaisesRegex(TypeError, 'Selector is not a tuple or a list'):
            base.Metadata().update({}, {'value': 'test'})

        with self.assertRaisesRegex(TypeError, 'is not a str, int, or ALL_ELEMENTS'):
            base.Metadata().update((1.0,), {'value': 'test'})

        with self.assertRaisesRegex(TypeError, 'is not a str, int, or ALL_ELEMENTS'):
            base.Metadata().update((None,), {'value': 'test'})

    def test_data(self):
        data = container.Dataset({
            '0': container.ndarray(numpy.array([
                [1, 2, 3],
                [4, 5, 6],
            ])),
        })

        md1 = base.DataMetadata(for_value=data, generate_metadata=False)

        with self.assertRaisesRegex(jsonschema.exceptions.ValidationError, 'is a required property'):
            md1.update((), {'value': 'test'})

        md1 = base.DataMetadata(for_value=data)

        md2 = md1.update((), {
            'id': 'test-dataset',
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': type(data),
            'dimension': {
                'length': 1
            }
        })

        md3 = md2.update(('0',), {
            'structural_type': type(data['0']),
            'dimension': {
                'length': 2
            }
        })

        self.assertEqual(md3.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'id': 'test-dataset',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.dataset.Dataset',
                'dimension': {
                    'name': 'resources',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
                    'length': 1
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 2,
                     'name': 'rows',
                     'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                },
               'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
               'structural_type': 'd3m.container.numpy.ndarray',
            },
        }, {
           'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata':  {
                'dimension': {
                    'length': 3,
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                },
            },
        }, {
           'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['0'],
            'metadata': {
                'structural_type': 'd3m.container.numpy.ndarray',
                'dimension': {
                    'length': 2,
                },
            },
        }])

    def test_prune_bug(self):
        metadata = base.Metadata().update((base.ALL_ELEMENTS, 0), {'foo': 'bar1'})
        metadata = metadata.update((0, 1), {'foo': 'bar2'})
        metadata = metadata.update((1, 1), {'foo': 'bar2'})
        metadata = metadata.update((2, 1), {'foo': 'bar2'})
        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'foo': 'bar3'})

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'foo': 'bar3'},
        }])

    def test_remove_empty_metadata(self):
        metadata = base.Metadata().update((base.ALL_ELEMENTS,), {
            'foo': {
                'bar': 42,
            },
            'other': 1,
        })

        metadata = metadata.update((base.ALL_ELEMENTS,), {
            'foo': {
                'bar': base.NO_VALUE,
            },
        })

        self.assertEqual(metadata.query((base.ALL_ELEMENTS,)), {
            'other': 1,
        })

        metadata = base.Metadata({
            'foo': {
                'bar': 42,
            },
            'other': 1,
        })

        metadata = metadata.update((), {
            'foo': {
                'bar': base.NO_VALUE,
            },
        })

        self.assertEqual(metadata.query(()), {
            'other': 1,
        })

        metadata = base.Metadata({
            'foo': {
                'bar': 42,
            },
        })

        metadata = metadata.update((), {
            'foo': {
                'bar': base.NO_VALUE,
            },
        })

        self.assertEqual(metadata.query(()), {})

        metadata = base.Metadata().update(('a',), {
            'foo': {
                'bar': 42,
            },
        })

        metadata = metadata.update((base.ALL_ELEMENTS,), {
            'foo': {
                'bar': base.NO_VALUE,
            },
        })

        self.assertEqual(metadata.query(('a',)), {})

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'foo': {
                    'bar': '__NO_VALUE__',
                },
            },
        }])

    def test_ignore_all_elements(self):
        metadata = base.Metadata().update((base.ALL_ELEMENTS,), {
            'foo': 'bar',
            'other': 42,
        })

        metadata = metadata.update((0,), {
            'foo': base.NO_VALUE,
        })

        metadata = metadata.update((2,), {
            'other2': 43,
        })

        self.assertEqual(metadata.query((0,)), {'other': 42})
        self.assertEqual(metadata.query((1,)), {'foo': 'bar', 'other': 42})
        self.assertEqual(metadata.query((2,)), {'foo': 'bar', 'other': 42, 'other2': 43})
        self.assertEqual(metadata.query((0,), ignore_all_elements=True), {})
        self.assertEqual(metadata.query((1,), ignore_all_elements=True), {})
        self.assertEqual(metadata.query((2,), ignore_all_elements=True), {'other2': 43})

        metadata = metadata.update((base.ALL_ELEMENTS,), {
            'foo': 'bar2',
        })

        self.assertEqual(metadata.query((0,)), {'foo': 'bar2', 'other': 42})
        self.assertEqual(metadata.query((1,)), {'foo': 'bar2', 'other': 42})
        self.assertEqual(metadata.query((2,)), {'foo': 'bar2', 'other': 42, 'other2': 43})
        self.assertEqual(metadata.query((0,), ignore_all_elements=True), {})
        self.assertEqual(metadata.query((1,), ignore_all_elements=True), {})
        self.assertEqual(metadata.query((2,), ignore_all_elements=True), {'other2': 43})

    def test_query_with_exceptions(self):
        metadata = base.Metadata().update((base.ALL_ELEMENTS,), {
            'foo': 'bar',
            'other': 42,
        })

        metadata = metadata.update((0,), {
            'foo': base.NO_VALUE,
        })

        metadata = metadata.update((2,), {
            'other2': 43,
        })

        self.assertEqual(metadata.query((0,)), {'other': 42})
        self.assertEqual(metadata.query((1,)), {'foo': 'bar', 'other': 42})
        self.assertEqual(metadata.query((2,)), {'foo': 'bar', 'other': 42, 'other2': 43})

        self.assertEqual(metadata.query_with_exceptions((0,)), ({'other': 42}, {}))
        self.assertEqual(metadata.query_with_exceptions((1,)), ({'foo': 'bar', 'other': 42}, {}))
        self.assertEqual(metadata.query_with_exceptions((2,)), ({'foo': 'bar', 'other': 42, 'other2': 43}, {}))

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS,)), ({
            'foo': 'bar',
            'other': 42,
        }, {
            (0,): {'other': 42},
            (2,): {'foo': 'bar', 'other': 42, 'other2': 43},
        }))

        metadata = metadata.update((base.ALL_ELEMENTS,), {
            'foo': 'bar2',
        })

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS,)), ({
            'foo': 'bar2',
            'other': 42,
        }, {
            (2,): {'foo': 'bar2', 'other': 42, 'other2': 43},
        }))

        metadata = base.Metadata().update((base.ALL_ELEMENTS, 0), {
            'name': 'bar',
        })

        metadata = metadata.update((base.ALL_ELEMENTS, 1), {
            'name': 'foo',
        })

        metadata = metadata.update((2, 0), {
            'name': 'bar2',
        })

        metadata = metadata.update((2, 2), {
            'name': 'foo2',
        })

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, 0)), ({
            'name': 'bar',
        }, {
            (2, 0): {'name': 'bar2'},
        }))

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, 1)), ({
            'name': 'foo',
        }, {}))

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, 2)), ({}, {
            (2, 2): {'name': 'foo2'},
        }))

        self.assertEqual(metadata.query_with_exceptions((2, base.ALL_ELEMENTS)), ({}, {
            (2, 0): {'name': 'bar2'},
            (2, 2): {'name': 'foo2'},
        }))

        metadata = base.Metadata().update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {
            'foo': 'bar',
            'other': 42,
        })

        metadata = metadata.update((base.ALL_ELEMENTS, 0), {
            'foo': base.NO_VALUE,
        })

        metadata = metadata.update((base.ALL_ELEMENTS, 2), {
            'other2': 43,
        })

        self.assertEqual(metadata.query((base.ALL_ELEMENTS, 0)), {'other': 42})
        self.assertEqual(metadata.query((base.ALL_ELEMENTS, 1)), {'foo': 'bar', 'other': 42})
        self.assertEqual(metadata.query((base.ALL_ELEMENTS, 2)), {'foo': 'bar', 'other': 42, 'other2': 43})

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, 0)), ({'other': 42}, {}))
        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, 1)), ({'foo': 'bar', 'other': 42}, {}))
        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, 2)), ({'foo': 'bar', 'other': 42, 'other2': 43}, {}))

        self.assertEqual(metadata.query_with_exceptions((base.ALL_ELEMENTS, base.ALL_ELEMENTS)), ({
            'foo': 'bar',
            'other': 42,
        }, {
            (base.ALL_ELEMENTS, 0): {'other': 42},
            (base.ALL_ELEMENTS, 2): {'foo': 'bar', 'other': 42, 'other2': 43},
        }))

    def test_semantic_types(self):
        metadata = base.DataMetadata({
            'structural_type': container.DataFrame,
            'schema': base.CONTAINER_SCHEMA_VERSION,
        })

        self.assertFalse(metadata.has_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/DatasetResource'))

        metadata = metadata.add_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/DatasetResource')

        self.assertTrue(metadata.has_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/DatasetResource'))

        metadata = metadata.remove_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/DatasetResource')

        self.assertFalse(metadata.has_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/DatasetResource'))

        metadata = metadata.add_semantic_type((base.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/Attribute')
        metadata = metadata.add_semantic_type((base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/PrimaryKey')
        metadata = metadata.add_semantic_type((base.ALL_ELEMENTS, 2), 'https://metadata.datadrivendiscovery.org/types/Attribute')

        self.assertEqual(metadata.get_elements_with_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/Attribute'), [])
        self.assertEqual(metadata.get_elements_with_semantic_type((base.ALL_ELEMENTS,), 'https://metadata.datadrivendiscovery.org/types/Attribute'), [0, 2])

    def test_copy_elements_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = copy_elements_metadata(metadata, target_metadata, ())

        self.assertTrue(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = copy_elements_metadata(metadata, target_metadata, ('level1',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = copy_elements_metadata(metadata, target_metadata, ('level1',), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['zlevel', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('zlevel', 'level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = copy_elements_metadata(metadata, target_metadata, ('level1', 'level2'))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = copy_elements_metadata(metadata, target_metadata, ('level1', 'level2'), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z'},
        }, {
            'selector': ['zlevel', 'level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

    def test_copy_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = metadata.copy_to(target_metadata, ())

        self.assertTrue(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0': 'foobar0',
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = metadata.copy_to(target_metadata, ('level1',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'level1': 'foobar1',
                'level1b': '__NO_VALUE__',
                'level1a': 'foobar1a',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = metadata.copy_to(target_metadata, ('level1',), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z', 'level1': 'foobar1', 'level1b': '__NO_VALUE__', 'level1a': 'foobar1a'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['zlevel', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['zlevel', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('zlevel', 'level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = metadata.copy_to(target_metadata, ('level1', 'level2'))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'level2': 'foobar2',
                'level2b': '__NO_VALUE__',
                'level2a': 'foobar2a',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        target_metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
        })

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})
        target_metadata = target_metadata.update(('zlevel',), {'level1z': 'foobar1z'})

        target_metadata = metadata.copy_to(target_metadata, ('level1', 'level2'), ('zlevel',))

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'level0z': 'foobar0z',
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['zlevel'],
            'metadata': {'level1z': 'foobar1z', 'level2': 'foobar2', 'level2b': '__NO_VALUE__', 'level2a': 'foobar2a'},
        }, {
            'selector': ['zlevel', 'level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['zlevel', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['zlevel', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['zlevel', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])


if __name__ == '__main__':
    unittest.main()
